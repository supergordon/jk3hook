#include "sdk.h"


CG_DrawStringExt_t pDrawStringExt = NULL;
CG_DrawHUD_t pDrawHud = NULL;
R_AddRefEntityToScene_t pAddRefEntityToScene = NULL;
CG_Trace_t pTrace = NULL;

centity_t* pCGE = NULL;;
cg_t* pCG = NULL;
cgs_t* pCGS = NULL;
DWORD dwPlayers = 0;

displayContextDef_t* pDC = NULL;


void GetPtrs()
{
	HMODULE hcgamex86 = NULL;
	while(!hcgamex86)
	{
		hcgamex86 = GetModuleHandle("cgamex86.dll");
		Sleep(100);
	}

	DWORD dw_vmMain = (DWORD)GetProcAddress(hcgamex86, "vmMain");

	pDC = (displayContextDef_t*)((DWORD)hcgamex86 + 0x6B6040);

	//$-5D0    >  81EC C8000000   sub esp,0C8
	DWORD dwInit = dw_vmMain - 0x5D0;

	pCGE = (centity_t*) *(DWORD*)(dwInit + 0x26);
	pCG = (cg_t*) *(DWORD*)(dwInit + 0x32);
	pCGS = (cgs_t*) *(DWORD*)(dwInit + 0x85);

	DWORD dwDrawHud = (DWORD)hcgamex86 + 0x23F40;
	pDrawHud = (CG_DrawHUD_t)DetourFunc((PBYTE)dwDrawHud, (PBYTE)hkDrawHUD, 5);

	DWORD dwAddRefEntityToScene = (DWORD)hcgamex86 + 0x5E820;
	pAddRefEntityToScene = (R_AddRefEntityToScene_t)DetourFunc((PBYTE)dwAddRefEntityToScene, (PBYTE)R_AddRefEntityToScene, 5);

	DWORD dwDrawStringExt = (DWORD)hcgamex86 + 0x2E2C0;
	pDrawStringExt = (CG_DrawStringExt_t)dwDrawStringExt;

	DWORD dwTrace = (DWORD)hcgamex86 + 0x56BA0;
	pTrace = (CG_Trace_t)dwTrace;

	dwPlayers = (DWORD)hcgamex86 + 0x51420;
	//pDC->Print("Hallo liebe Leute");
}

bool __stdcall DllMain(HANDLE, DWORD dwReason, LPVOID)
{
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)GetPtrs, 0, 0, 0);
	}

	return true;
}