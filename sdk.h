#ifndef _SDK_H
#define _SDK_H

#pragma warning (disable: 4267 4311 4312)

#include <windows.h>
#include <fstream>
#include "SDK/cgame/cg_local.h"
#include "SDK/ui/ui_shared.h"
#include "tools.h"

extern cg_t* pCG;
extern cgs_t* pCGS;
extern centity_t* pCGE; 

extern  DWORD dwPlayers;

extern "C" void * _ReturnAddress(void);
#pragma intrinsic(_ReturnAddress)

extern void __cdecl hkDrawHUD(centity_t* cent);
extern void __cdecl R_AddRefEntityToScene(refEntity_t* re);

typedef void (__cdecl* CG_DrawHUD_t)(centity_t* cent);
typedef void (__cdecl* CG_DrawStringExt_t)(int, int, char*, float*, qboolean, qboolean, int, int, int);
typedef void (__cdecl* R_AddRefEntityToScene_t)(refEntity_t *re);
typedef void (__cdecl* CG_Trace_t)(trace_t*, vec3_t, vec3_t, vec3_t, vec3_t, int, int);

extern CG_DrawStringExt_t pDrawStringExt;
extern CG_DrawHUD_t pDrawHud;
extern R_AddRefEntityToScene_t pAddRefEntityToScene;
extern CG_Trace_t pTrace;
extern displayContextDef_t* pDC;

#endif


//cgamex86.dll
//$+25     >  BF 80187430     mov edi,cgamex86.30741880                ; cg_entities
//$+31     >  BF 00926F30     mov edi,cgamex86.306F9200                ; cg
//$+3D     >  BF E02B6F30     mov edi,cgamex86.306F2BE0                ; cg_items
//$+84     >  BF A06A6B30     mov edi,cgamex86.306B6AA0                ; cgs
//$+90     >  BF E04A6B30     mov edi,cgamex86.306B4AE0                ; cg_weapons
//30000000