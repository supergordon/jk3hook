#include "sdk.h"

void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);

	memcpy(jmp, src, len);	jmp += len;

	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;

	VirtualProtect(src, len, dwback, &dwback);

	return (jmp-len);
}

void AngleVectors( const vec3_t angles, vec3_t forward, vec3_t right, vec3_t up) {
	float	angle;
	static float	sr, sp, sy, cr, cp, cy;

	angle = angles[YAW] * (M_PI*2 / 360);
	sy = sin(angle);
	cy = cos(angle);
	angle = angles[PITCH] * (M_PI*2 / 360);
	sp = sin(angle);
	cp = cos(angle);
	angle = angles[ROLL] * (M_PI*2 / 360);
	sr = sin(angle);
	cr = cos(angle);

	if (forward){
		forward[0] = cp*cy;
		forward[1] = cp*sy;
		forward[2] = -sp;
	}
	if (right){
		right[0] = (-1*sr*sp*cy+-1*cr*-sy);
		right[1] = (-1*sr*sp*sy+-1*cr*cy);
		right[2] = -1*sr*cp;
	}
	if (up){
		up[0] = (cr*sp*cy+-sr*-sy);
		up[1] = (cr*sp*sy+-sr*cy);
		up[2] = cr*cp;
	}
}

qboolean qWorldToScreen(vec3_t worldCoord, int *x, int *y)
{
	int	xcenter, ycenter;
	vec3_t	local, transformed, vfwd, vright, vup;

	xcenter = 640 / 2;
	ycenter = 480 / 2;

	AngleVectors(pCG->refdef.viewangles, vfwd, vright, vup);
	VectorSubtract (worldCoord, pCG->refdef.vieworg, local);

	transformed[0] = DotProduct(local,vright);
	transformed[1] = DotProduct(local,vup);
	transformed[2] = DotProduct(local,vfwd);		

	if(transformed[2] < 0.01) return qfalse;

	float xzi = xcenter / transformed[2] * (96.0f/pCG->refdef.fov_x);
	float yzi = ycenter / transformed[2] * (102.0f/pCG->refdef.fov_y);

	*x = xcenter + xzi * transformed[0];
	*y = ycenter - yzi * transformed[1];

	return qtrue;
}

qboolean qIsVisible(vec3_t start, vec3_t end)
{
	trace_t tr;
	pTrace(&tr, start, 0, 0, end, -1, CONTENTS_SOLID|CONTENTS_BODY);
	if(tr.fraction >= 0.85f)
		return qtrue;

	return qfalse;
}

qboolean qIsPlayer(centity_t* cent)
{
	if(cent->currentState.clientNum > MAX_CLIENTS) return qfalse;
	if(cent->currentState.eType != ET_PLAYER  || cent->currentState.eFlags & EF_DEAD )	return qfalse;

	return qtrue;
}

void DrawString(int x, int y, int r, int g, int b, int size, char* szText)
{
	float color[4] = {r/255, g/255, b/255, 1.0f};
	pDrawStringExt(x, y, szText, color, qfalse, qfalse, size, size+size, 0);
}