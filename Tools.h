#ifndef _TOOLS_
#define _TOOLS_

extern void *DetourFunc(BYTE *src, const BYTE *dst, const int len);

extern qboolean qWorldToScreen(vec3_t worldCoord, int *x, int *y);
extern qboolean qIsVisible(vec3_t start, vec3_t end);
extern qboolean qIsPlayer(centity_t* cent);
extern void DrawString(int x, int y, int r, int g, int b, int size, char* szText);

#endif