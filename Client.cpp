#include "sdk.h"

struct player
{
	DWORD time;
	vec3_t origin;
}; player players[32] = {0};

vec3_t me2 = {0};

void __cdecl hkDrawHUD(centity_t* cent)
{
	int x = 0, y = 0;
	
	for(int i = 0; i < pCGS->maxclients; i++)
	{
		if(pCGS->clientinfo[i].name[0] && qIsPlayer(&pCGE[i]) && i != pCG->snap->ps.clientNum)
		{
			if(players[i].origin[0] != pCGE[i].lerpOrigin[0] || players[i].origin[1] != pCGE[i].lerpOrigin[1] || players[i].origin[2] != pCGE[i].lerpOrigin[2]) 
				players[i].time = GetTickCount();
			
			vec3_t origin = {pCGE[i].lerpOrigin[0], pCGE[i].lerpOrigin[1], pCGE[i].lerpOrigin[2]+57};
			vec3_t me = {cent->lerpOrigin[0], cent->lerpOrigin[1], cent->lerpOrigin[2]+45};
			
			if(qWorldToScreen(origin, &x, &y) && (players[i].time+2200) > GetTickCount())
			{				
				int center = pDC->textWidth(pCGS->clientinfo[i].name, 0.5f, FONT_MEDIUM); 
				center /= 2;
				
				//pCGE[i].playerState->weapon
				
				if(qIsVisible(me, origin))
					DrawString(x-center, y, 0, 255, 0, 4, pCGS->clientinfo[i].name);
				else
					DrawString(x-center, y, 255, 0, 0, 4, pCGS->clientinfo[i].name);
			}
			VectorCopy(pCGE[i].lerpOrigin, players[i].origin);
			VectorCopy(cent->lerpOrigin, me2);
		}
	}

	DrawString(10, 20, 0, 255, 0, 5, "Client Hook by Gordon`");

	return pDrawHud(cent);
}


void __cdecl R_AddRefEntityToScene(refEntity_t* re)
{
	if((DWORD)_ReturnAddress() > dwPlayers && (DWORD)_ReturnAddress() < (dwPlayers+0x4723))
	{
		pAddRefEntityToScene(re);
		re->renderfx = RF_NODEPTH;

	/*	if(!qIsVisible(me2, re->origin))
		{
			re->shaderRGBA[0] = 255;
			re->shaderRGBA[1] = 0;
			re->shaderRGBA[2] = 0;
			re->shaderRGBA[3] = 255;
			re->customShader = pCGS->media.sightShell;
		}*/
	}

	pAddRefEntityToScene(re);
}
